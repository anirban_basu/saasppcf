package saasppcf.webapp.common.util;

import java.math.BigInteger;

public class StringUtilities {
	//public static final String SLOPE_ONE_TUPLE_SEPARATOR = "::::";
	public static final String QUEUENAME_DATASETS = "datasets";
	public static final int RADIX_HEX = 16;
	/**
	 * Converts an arbitrary length hexadecimal number, positive or negative
	 * to an arbitrary precision BigInteger.
	 * @param hex
	 * @return
	 */
	public static BigInteger hexStringToBigInteger(String hex) {
		BigInteger result = BigInteger.ZERO;
		boolean negative = false;
		long intermediate = 0L;
		int cursor = 0;
		if(hex.charAt(0)=='-') {
			negative = true;
			cursor++;
		}
		while(cursor<hex.length()) {
			intermediate = Long.parseLong(Character.toString(hex.charAt(cursor)),16); //hexadecimal to long
			result = result.multiply(BigInteger.valueOf(16));
			result = result.add(BigInteger.valueOf(intermediate));
			cursor++;
		}
		if(negative) {
			result = BigInteger.ZERO.subtract(result);
		}
		return result;
	}
	
	public static String generateUniqueID(String str1, String str2) {
		BigInteger bA = BigInteger.ZERO;
		BigInteger bB = BigInteger.ZERO;
		for(int i=0; i<str1.length(); i++) {
			bA = bA.add(BigInteger.valueOf(127L).pow(i+1).multiply(BigInteger.valueOf(str1.codePointAt(i))));
		}
		for(int i=0; i<str2.length(); i++) {
			bB = bB.add(BigInteger.valueOf(127L).pow(i+1).multiply(BigInteger.valueOf(str2.codePointAt(i))));
		}
		return bA.multiply(bB).toString(RADIX_HEX);
	}
}
