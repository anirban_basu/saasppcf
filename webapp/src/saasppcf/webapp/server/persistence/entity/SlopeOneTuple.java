package saasppcf.webapp.server.persistence.entity;

import java.io.Serializable;

import javax.jdo.annotations.Extension;
import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;

import saasppcf.webapp.common.util.StringUtilities;

@Entity
@Cacheable(true)
public class SlopeOneTuple implements Serializable {
	public SlopeOneTuple(String firstItem, String secondItem, double deviation,
			long cardinality) {
		super();
		this.firstItem = firstItem;
		this.secondItem = secondItem;
		this.deviation = deviation;
		this.cardinality = cardinality;
		this._compositeKey = StringUtilities.generateUniqueID(firstItem, secondItem);
	}

	private static final long serialVersionUID = 2L;
	
	@Id
	private String _compositeKey;
	
	@Extension(vendorName="datanucleus", key="gae.unindexed", value="true")
	private String firstItem;
	
	@Extension(vendorName="datanucleus", key="gae.unindexed", value="true")
	private String secondItem;
	
	@Extension(vendorName="datanucleus", key="gae.unindexed", value="true")
	private double deviation;
	
	@Extension(vendorName="datanucleus", key="gae.unindexed", value="true")
	private long cardinality;
	
	@Extension(vendorName="datanucleus", key="gae.unindexed", value="true")
	private String dataset;

	public String get_compositeKey() {
		return _compositeKey;
	}

	public void set_compositeKey(String _compositeKey) {
		this._compositeKey = _compositeKey;
	}

	public String getFirstItem() {
		return firstItem;
	}

	public void setFirstItem(String firstItem) {
		this.firstItem = firstItem;
		this._compositeKey = StringUtilities.generateUniqueID(firstItem, secondItem);
	}

	public String getSecondItem() {
		return secondItem;
	}

	public void setSecondItem(String secondItem) {
		this.secondItem = secondItem;
		this._compositeKey = StringUtilities.generateUniqueID(firstItem, secondItem);
	}

	public double getDeviation() {
		return deviation;
	}

	public void setDeviation(double deviation) {
		this.deviation = deviation;
	}

	public long getCardinality() {
		return cardinality;
	}

	public void setCardinality(long cardinality) {
		this.cardinality = cardinality;
	}

	public String getDataset() {
		return dataset;
	}

	public void setDataset(String dataset) {
		this.dataset = dataset;
	}
	
	public String toString() {
		return new String(
				this.getClass().getName() + " {" +
				this._compositeKey + ", " + 
				this.firstItem + ", " + 
				this.secondItem + ", " + 
				this.deviation + ", " + 
				this.cardinality + ", " + 
				this.dataset + "}"
				);
	}
	
}
