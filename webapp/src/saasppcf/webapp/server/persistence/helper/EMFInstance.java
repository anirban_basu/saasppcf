package saasppcf.webapp.server.persistence.helper;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class EMFInstance {
	private static final String TRANSACTIONS_OPTIONAL = "transactions-optional";
	private static final String TRANSACTIONS_OPTIONAL_JCACHE = "transactions-optional-jcache";
    private static final EntityManagerFactory emfWithCacheInstance =
        Persistence.createEntityManagerFactory(TRANSACTIONS_OPTIONAL_JCACHE);
    private static final EntityManagerFactory emfInstance =
            Persistence.createEntityManagerFactory(TRANSACTIONS_OPTIONAL);

    private EMFInstance() {}

    public static EntityManagerFactory getWithCache() {
        return emfWithCacheInstance;
    }
    
    public static EntityManagerFactory get() {
        return emfInstance;
    }
}
