package saasppcf.webapp.server.persistence.helper;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.RollbackException;

import saasppcf.webapp.common.util.StringUtilities;
import saasppcf.webapp.server.persistence.entity.SlopeOneTuple;

public class SlopeOneTupleAccessor {
	protected EntityManager manager;
	
	protected SlopeOneTupleAccessor(EntityManager manager) {
		this.manager = manager;
	}

	/**
	 * Obtains a SlopeOne tuple the backend storage.
	 *
	 * 
	 * @param firstItem
	 * @param secondItem
	 * @return
	 */
	public SlopeOneTuple get(String firstItem, String secondItem) {
		String compositeKey = StringUtilities.generateUniqueID(firstItem, secondItem).toString();
		SlopeOneTuple find = manager.find(SlopeOneTuple.class, compositeKey);
		if(find!=null) {
			if(find.getSecondItem().compareTo(firstItem)==0) {
				//there has been a reversal, so set the deviation accordingly
				find.setDeviation(0-find.getDeviation());
			}
		}
		return find;
	}

	/**
	 * Write (first time or overwrite) a SlopeOne tuple to the 
	 * datastore in a transaction.
	 * 
	 * @param tuple
	 */
	public boolean put(SlopeOneTuple tuple) {
		String compositeKey = StringUtilities.generateUniqueID(tuple.getFirstItem(), tuple.getSecondItem()).toString();
		SlopeOneTuple find = manager.find(SlopeOneTuple.class, compositeKey);
		EntityTransaction txn = manager.getTransaction();
		txn.begin();
		if(find!=null) {
			find.setCardinality(tuple.getCardinality());
			if(find.getSecondItem().compareTo(tuple.getFirstItem())==0) {
				//there has been a reversal, so set the deviation accordingly
				find.setDeviation(0-tuple.getDeviation());
			}
			else {
				find.setDeviation(tuple.getDeviation());
			}
		}
		else {
			manager.persist(find);
		}
		try {
			txn.commit();
		}
		catch(RollbackException e) {
			return false;
		}
		return true;
	}

	/**
	 * Add a SlopeOne tuple to the existing tuple in the 
	 * datastore identifiable by the item-pair combination.
	 * 
	 * @param tuple
	 */
	public boolean add(SlopeOneTuple tuple) {
		String compositeKey = StringUtilities.generateUniqueID(tuple.getFirstItem(), tuple.getSecondItem()).toString();
		SlopeOneTuple find = manager.find(SlopeOneTuple.class, compositeKey);
		EntityTransaction txn = manager.getTransaction();
		txn.begin();
		if(find!=null) {
			find.setCardinality(find.getCardinality() + tuple.getCardinality());
			if(find.getSecondItem().compareTo(tuple.getFirstItem())==0) {
				//there has been a reversal, so set the deviation accordingly
				find.setDeviation(0 - find.getDeviation() - tuple.getDeviation());
			}
			else {
				find.setDeviation(find.getDeviation() + tuple.getDeviation());
			}
		}
		else {
			manager.persist(find);
		}
		try {
			txn.commit();
		}
		catch(RollbackException e) {
			return false;
		}
		return true;
	}

	/**
	 * Deletes a SlopeOne tuple for an item-item pair from the datastore.
	 */
	public boolean remove(SlopeOneTuple tuple) {
		String compositeKey = StringUtilities.generateUniqueID(tuple.getFirstItem(), tuple.getSecondItem()).toString();
		SlopeOneTuple find = manager.find(SlopeOneTuple.class, compositeKey);
		if(find!=null) {
			EntityTransaction txn = manager.getTransaction();
			txn.begin();
			manager.remove(find);
			try {
				txn.commit();
			}
			catch(RollbackException e) {
				return false;
			}
		}
		else {
			return false;
		}
		return true;
	}

}
