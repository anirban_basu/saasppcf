package saasppcf.crypto.impl.paillier;

import java.math.BigInteger;

import saasppcf.crypto.design.PrivateKeyDO;

/**
 * A private key of the Paillier cryptosystem. Note that the private key is a
 * subclass of the public key.
 * 
 * @author Anirban Basu
 *
 */
public class PaillierPrivateKeyDO extends PaillierPublicKeyDO implements PrivateKeyDO {
	public PaillierPrivateKeyDO() {}
	
	public PaillierPrivateKeyDO(BigInteger n, BigInteger halfN,
			BigInteger nSquared, int bitSize, BigInteger lambda, BigInteger mu) {
		super(n, halfN, nSquared, bitSize);
		this.lambda = lambda;
		this.mu = mu;
	}
	
	private static final long serialVersionUID = 1L;
	protected BigInteger lambda = null, mu = null;
	public BigInteger getLambda() {
		return lambda;
	}
	public void setLambda(BigInteger lambda) {
		this.lambda = lambda;
	}
	public BigInteger getMu() {
		return mu;
	}
	public void setMu(BigInteger mu) {
		this.mu = mu;
	}
}
