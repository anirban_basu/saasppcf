package saasppcf.crypto.design;

import java.io.Serializable;

/**
 * A Serializable placeholder interface for the private key of
 * the homomorphic cryptosystem.
 * @author Anirban Basu
 *
 */

public interface PrivateKeyDO extends Serializable {

}
