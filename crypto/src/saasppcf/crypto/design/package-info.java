/**
 * This package is a collection of generic homomorphic cryptosystem interfaces which are
 * implemented by concrete implementations of such cryptosystems.
 * 
 * @author Anirban Basu
 */

package saasppcf.crypto.design;