package saasppcf.crypto.design;

import java.io.Serializable;

/**
 * A Serializable placeholder interface for the public key of
 * the homomorphic cryptosystem.
 * @author Anirban Basu
 *
 */
public interface PublicKeyDO extends Serializable {

}
